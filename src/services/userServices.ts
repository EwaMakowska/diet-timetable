import axios, { AxiosResponse } from "axios";

const baseURL: string = "../fakedata/";

export function getUserById(id) {
    return axios.get(baseURL+"users.json").then((response: AxiosResponse) => {

        let user = response.data.filter( (item) => { return item.id === id })[0]
        return user
        
    });
}