import axios, { AxiosResponse } from "axios";

const baseURL: string = "../fakedata/";

export function getMenuItems() {
    return axios.get(baseURL+"menu.json").then((response: AxiosResponse) => {
        return response
    });
}