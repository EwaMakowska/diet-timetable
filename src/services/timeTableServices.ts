import axios, { AxiosResponse } from "axios";

const baseURL: string = "../fakedata/";

export function getUserTimetable(userId: number) {
    return axios.get(baseURL+"timetable.json").then((response: AxiosResponse) => {

        let timetable = response.data.filter( (item) => { 
            if(item.userId === userId) {
                return item
            }
         })[0]

        return timetable.timetable[0]
        
    });
    
}