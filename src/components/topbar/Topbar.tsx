import Menu from './menu/Menu';
import UserMenu from './userMenu/UserMenu';

import './topbar.css'

function Topbar() {
  return (
    <div className="topbar shadow">
      <Menu />
      <UserMenu />
    </div>
  );
}

export default Topbar;
