import React from 'react';

import { getUserById } from "../../../services/userServices"

import UserTypes from '../../../types/userTypes';

import './userMenu.css';

export default function UserMenu() {
  const [user, setUser] = React.useState<UserTypes>({
    id: null,
    surname: "",
    name: "",
    avatar: ""
  });

  React.useEffect(() => {
    getUserById(123).then((response) => {
      setUser(response)
    })
  }, [user.name, user.surname, user.avatar]);

  if (!user) return null;
  
  return (
    <div className="userMenu">
      <div className="avatar shadow" style={{backgroundImage: `url(${user.avatar})`}}></div>
      <div className='userBar'>{user.name} {user.surname}</div>
    </div>
  );
}
