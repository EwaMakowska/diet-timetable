import React from 'react';

import { getMenuItems } from "../../../services/menuServices"

import { isActive } from '../../../utils/utils';

import MenuType from '../../../types/menuTypes';

import './menu.css'

export default function Menu() {
    const [items, setItems] = React.useState<MenuType[] | null>(null);

    React.useEffect(() => {

        getMenuItems().then((response): void => {
            setItems(response.data)
        })
    }, []);
  
    if (!items) return null;
    
    return (
      <ul className="menu">
        {items.map((item, index) => (
            <li className={isActive(item.isActive)} key={index}>
                <a href="/#">{item.name}</a>
            </li>
        ))}
      </ul>
    );
  }