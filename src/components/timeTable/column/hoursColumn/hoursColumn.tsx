import React from 'react';

import './hoursColumn.css'

export default function HoursColumn() {
    const [hours] = React.useState<string[]>(
        ["6:00 AM","9:00 AM", "12:00 PM", "3:00 PM", "6:00 PM"]
      )

      const isLastRow = (index: number): string => {
          return (index+1 === hours.length)?'lastRow': ''
      }

    return (
        <div className='hoursColumn'>
            {hours.map((hour, index) =>(
                console.log(`index`, index, hours.length),

                <div className={`eventBorder ${isLastRow(index)}`} key={index}>{hour}</div>
            ))}
            <div className="workoutDescription">
                <div className="label">workout</div>
            </div>
        </div>
    )
}