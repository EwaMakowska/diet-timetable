import React from 'react';

import './tableHeader.css'

export default function TableHeader(props) {

    return (
        <div className='tableHeader backgroundWhite eventBorder'>
            day {props.day}
        </div>
    )
}