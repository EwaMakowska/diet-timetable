import React from 'react';

import { isActive } from '../../../../utils/utils';

import './workout.css'

export default function Workout(props) {

    return (
        <div className='workout'>
            <img className={`image ${isActive(props.workout)}`} src="../icons/workout.png" alt="" />
            {(props.workout)
                ?<img src="../icons/ok.png" className="icon" alt="ok icon"/>
                :""}
        </div>
    )
}