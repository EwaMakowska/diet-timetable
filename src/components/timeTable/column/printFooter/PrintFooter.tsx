import React from 'react';

import './printFooter.css'

export default function PrintFooter() {

    return (
        <div className='printFooter'>
            <img src="../icons/print.svg" alt="print icon" /> <p>print</p>
        </div>
    )
}