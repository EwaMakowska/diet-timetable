import React from 'react';

import Event from '../event/Event';
import TableHeader from './tableHeader/TableHeader';
import TableFooter from './tableFooter/TableFooter';
import Workout from './workout/Workout';
import PrintFooter from './printFooter/PrintFooter';
import TimetableTypes from "../../../types/timetableTypes"
import GuiltFree from '../event/guiltFree/GuiltFree';

import './column.css'

export default function Column(dayliEvents: TimetableTypes) {

    const [activeDay] = React.useState<number>(65)

    const createColumns = () => {
        return (dayliEvents.events.map((event, index) => ([
            <Event {...event} key={index} />,
        ])))
    }

    const isGuiltFreeColumn = (): string => {
        return (dayliEvents.events.length === 0)? "guiltFreeColumn": "" 
    }

    const isLastColumn = (): string | null => {
        return (Number(dayliEvents['day']) % 7 === 0)? 'lastColumn': ""
    }

    const isActiveDay = (): string => {
        if(Number(dayliEvents['day']) === activeDay - 1) {
            return 'prevDay'
        }
        if(Number(dayliEvents['day']) === activeDay) {
            return 'active'
        }
        return ''
    }

    return (
        <div className={`column ${isGuiltFreeColumn()} ${isActiveDay()} ${isLastColumn()}`}>
            <TableHeader day={dayliEvents['day']} />
            {(dayliEvents['events'].length === 0)
                ? <GuiltFree />
                : createColumns()
            }
            <PrintFooter />
            <TableFooter type={dayliEvents['type']} />
            <Workout workout={dayliEvents['workout']} />
        </div>
    )
}