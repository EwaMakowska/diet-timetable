import React from 'react';

import { getUserTimetable } from "../../services/timeTableServices"

import Toolbar from './toolbar/Toolbar';
import HoursColumn from './column/hoursColumn/hoursColumn';
import Column from './column/Column';

import TimetableTypes from "../../types/timetableTypes"

import './timeTable.css'

export default function TimeTable() {

  const [events, setEvents] = React.useState<TimetableTypes[] | null>(null);
  const [activeDay] = React.useState<number>(65)

  React.useEffect(() => {
    getUserTimetable(123).then((response) => {
      setEvents(response);
    })
  }, []);

  if (!events) return null;



  return (
    <div className='timeTable'>
      <Toolbar activeDay={activeDay} />
      <div className='tableBody'>
      <HoursColumn />
      {events.map((dayliEvents, index) => (
        <Column {...dayliEvents} key={index} />
      ))}
      </div>
    </div>
  )
}