import React from 'react';

import Description from '../description/Description';

import {isActive} from "../../../../utils/utils"

import './proteinsOptions.css'

export default function ProteinsOptions() {

    const [options] = React.useState([
        {
            state: true,
            image: "../proteinsOptions/broccolia.png",
        },
        {
            state: true,
            image: "../proteinsOptions/chees.png",
        },
        {
            state: true,
            image: "../proteinsOptions/meat.png",
        },
        {
            state: false,
            image: "../proteinsOptions/fish.png",
        },
        {
            state: false,
            image: "../proteinsOptions/chicken.png",
        },
    ]);

    return (
        <div className='proteinsOptions'>
            <Description props="select your protein options"/>
            <div className="options">
                {
                    options.map((option, index) => (
                        <div className={`option ${isActive(option.state)}`} key={'option'+index} style={{backgroundImage: `url(${option.image})`}}>
                            <img src="../proteinsOptions/crossOut.png" alt="no active" />
                        </div>
                    ))
                }
            </div>
        </div>
    )
}