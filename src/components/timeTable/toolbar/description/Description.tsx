import React from 'react';

import DefaultPropsTypes from '../../../../types/defaultPropsType';

import './description.css'

export default function Description(props: DefaultPropsTypes) {

    return (
        <p className='description'>
            {props.props}
        </p>
    )
}