
import DaySwitcher from './daySwitcher/DaySwitcher';
import ProgressBar from './progressBar/ProgressBar';
import ProteinsOptions from './proteinsOptions/ProteinsOptions';

import './toolbar.css'

export default function Toolbar(props) {

    return (
        <div className='toolbar'>
            <ProgressBar />
            <DaySwitcher activeDay={props.activeDay} />
            <ProteinsOptions />            
        </div>
    )
}