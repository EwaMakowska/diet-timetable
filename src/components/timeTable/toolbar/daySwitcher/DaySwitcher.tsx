import React from 'react';

import './daySwitcher.css'

export default function DaySwitcher(props) {

    const [currentWeek, setCurrentWeek] = React.useState<number | null>(null)

    const prev = () => {

        if(currentWeek && currentWeek > 1) {
            let week = currentWeek - 1;
            setCurrentWeek(week);
        }
    }

    const next = (): void => {
        if(currentWeek) {
        let week = currentWeek + 1;
        setCurrentWeek(week);            
        }
    }

    React.useEffect(() => {
        if(!currentWeek) {
            let week = Math.trunc((props.activeDay / 7) + 1)
            setCurrentWeek(week)
        }
        
    }, [currentWeek]);
  
    if (!currentWeek) return null;


    return (
        <div className='daySwitcher'>
            <button className="button prev" onClick={prev}></button>
            <h1>Week {currentWeek}</h1>
            <button className="button next" onClick={next}></button>
        </div>
      )
  }