import React from 'react';

import Description from '../description/Description';

import './progressBar.css'

export default function ProgressBar() {

    const [elementsNumber] = React.useState<number>(12);
    const [activedElementsNumber] = React.useState<number>(6);

    const createElements = (): JSX.Element[] => {
        let elements: JSX.Element[] = []

    const markActivated = (i: number):string => {
        if(i < activedElementsNumber) {
            return 'active'
        }
        if(i === activedElementsNumber) {
            return 'next'
        }
        return ''
    }

        for(let i = 0; i < elementsNumber; i++) {
            elements.push(
            <div className="wrapper" key={"wrapper"+i}>
                <div className='top'>
                    <div className={`line ${markActivated(i)}`}></div>                    
                    <div className={`circle ${markActivated(i)}`}>
                        <div className='number'>{i+1}</div>
                    </div>
                </div>
            </div>
            )
        }
        return elements
    }

    return (
        <div className='progressBar'>
            <Description props="your 12 week progress"/>
            <div className='elements'>
                {createElements()}
            </div>
        </div>
      )
  }