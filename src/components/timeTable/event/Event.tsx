import EventTypes from "../../../types/eventTypes"

import { isActive } from "../../../utils/utils"

import './event.css'

export default function Event(event?: EventTypes) {

    return (<div className="event backgroundWhite eventBorder">
                <div className="productBar">
                    <div className='eventProduct'>{event?.product}</div>
                    <img className={`icon ${isActive(event?.done)}`} src="../icons/circleOK.png" alt="ok icon"/>
                </div>
                <div className="image" style={{backgroundImage: `url(${event?.image})`}}></div>     
            </div>
            )
}