import React from 'react';
import './App.css';

import Topbar from './components/topbar/Topbar';
import TimeTable from './components/timeTable/TimeTable';

function App() {
  return (
    <div className="App">
      <Topbar />
      <TimeTable />
    </div>
  );
}

export default App;
