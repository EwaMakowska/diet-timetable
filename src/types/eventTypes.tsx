export default interface EventTypes {
    product: string,
    done: boolean,
    image?: string,
    guiltFreeDay: boolean,
}