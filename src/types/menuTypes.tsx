export default interface ItemsType {
    name: string,
    isActive: boolean
}