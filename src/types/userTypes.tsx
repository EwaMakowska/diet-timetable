export default interface UserTypes {
    id: number | null;
    surname: string;
    name: string;
    avatar: string;
}