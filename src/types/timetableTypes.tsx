import EventTypes from './eventTypes'

export default interface TimetableTypes {
    day: string,
    type?: string,
    workout?: boolean,
    events: EventTypes[]
}